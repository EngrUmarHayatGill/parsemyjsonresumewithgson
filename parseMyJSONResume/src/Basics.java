import java.util.ArrayList;


public class Basics {
private String name;
private String label;
private String picture;
private String email;
private String phone;
private String website;
private String summary;
private Location location;
private ArrayList<Profile> profiles;
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getLabel() {
	return label;
}
public void setLabel(String label) {
	this.label = label;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getPhone() {
	return phone;
}
public void setPhone(String phone) {
	this.phone = phone;
}
public String getPicture() {
	return picture;
}
public void setPicture(String picture) {
	this.picture = picture;
}
public String getSummary() {
	return summary;
}
public void setSummary(String summary) {
	this.summary = summary;
}
public String getWebsite() {
	return website;
}
public void setWebsite(String website) {
	this.website = website;
}
public Location getLocation() {
	return location;
}
public void setLocation(Location location) {
	this.location = location;
}
public ArrayList<Profile> getProfiles() {
	return profiles;
}
public void setProfiles(ArrayList<Profile> profiles) {
	this.profiles = profiles;
}
}
